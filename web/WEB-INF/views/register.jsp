<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored ="false" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<%@ include file="common/head.jsp"%>

<body>

<%@ include file="common/header.jsp"%>

<main role="main">

    <div class="container">
        <div class="col-md-12">

            <br>
            <h3>Sign In</h3>
            <br>

            <spring:form method="post" action="register" modelAttribute="user">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </spring:form>

        </div>

        <hr>
    </div>

</main>

<%@ include file="common/footer.jsp"%>
<%@ include file="common/scripts.jsp"%>

</body>
</html>


