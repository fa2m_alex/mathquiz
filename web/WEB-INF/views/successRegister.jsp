<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored ="false" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<%@ include file="common/head.jsp"%>

<body>

<%@ include file="common/header.jsp"%>

<main role="main">

    <div class="container">
        <div class="col-md-12">

            <br>

            Successful registration!

        </div>

        <hr>
    </div>

</main>

<%@ include file="common/footer.jsp"%>
<%@ include file="common/scripts.jsp"%>

</body>
</html>


