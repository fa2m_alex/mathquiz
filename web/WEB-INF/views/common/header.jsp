<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="/">Math Quiz</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/profile">Profile</a>
            </li>
        </ul>

        <%@ page import = "com.selivanov.controller.TaskController" %>

        <c:choose>
            <c:when test="${!TaskController.login}">
                <a class="btn btn-outline-light my-2 my-sm-0" href="/login" role="button">Log In</a>
            </c:when>
            <c:otherwise>
                <a class="btn btn-outline-light my-2 my-sm-0" href="/logout" role="button">Log Out</a>
            </c:otherwise>
        </c:choose>

    </div>
</nav>