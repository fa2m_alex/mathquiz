<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored ="false" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<%@ include file="common/head.jsp"%>

<body>

<%@ include file="common/header.jsp"%>

<main role="main">

    <div class="container">
        <div class="col-md-12">

            <br>
            <h3>Learn ${task_def}</h3>

            <spring:form modelAttribute="data" action="task" method="post">
                <c:forEach items="${data.tasks}" varStatus="status" var="task">
                    <spring:hidden path="tasks[${status.index}].correctAnswer" title="${task.correctAnswer}" />
                    <spring:hidden path="tasks[${status.index}].task" title="${task.task}" />
                    <spring:hidden path="tasks[${status.index}].individual" title="${task.individual}"></spring:hidden>

                    <div class="card">
                        <div class="card-header">${task.task}</div>
                        <div class="card-body">
                            <c:forEach items="${task.answers}" var="tempAnswer">
                                <div class="form-check">
                                    <spring:radiobutton cssClass="form-check-input" path="tasks[${status.index}].answer" value="${tempAnswer}"/>
                                    <label class="form-check-label" for="${tempAnswer}">
                                            ${tempAnswer}
                                    </label>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <br>
                </c:forEach>
                <button type="submit" class="btn btn-primary">Submit</button>
            </spring:form>
        </div>

        <hr>
    </div>

</main>

<%@ include file="common/footer.jsp"%>
<%@ include file="common/scripts.jsp"%>

</body>
</html>


