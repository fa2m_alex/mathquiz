<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">

<%@ include file="common/head.jsp"%>

<body>

<%@ include file="common/header.jsp"%>

<main role="main">

    <div class="container">
        <div class="col-md-12">
            <br>
            <h1>Error</h1>
        </div>

        <hr>

    </div> <!-- /container -->

</main>

<%@ include file="common/footer.jsp"%>
<%@ include file="common/scripts.jsp"%>

</body>
</html>

