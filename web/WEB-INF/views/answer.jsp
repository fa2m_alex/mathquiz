<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<%@ include file="common/head.jsp"%>
<body>
<%@ include file="common/header.jsp"%>
<main>
    <div class="container">
        <div class="col-md-12">
            <br>
            <h3>Results</h3>
            <c:forEach var="task" items="${data.tasks}">
                <div class="card">
                    <div class="card-header">${task.task}</div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Your answer is ${task.answer}</li>

                            <c:choose>
                                <c:when test="${task.correct}">
                                    <li class="list-group-item bg-success text-white">Correct</li>
                                </c:when>
                                <c:otherwise>
                                    <li class="list-group-item bg-danger text-white">
                                        Wrong! <a class="btn btn-dark" href="/learn?task_id=${task.individual}" role="button">Learn</a>
                                    </li>
                                </c:otherwise>
                            </c:choose>

                    </ul>
                </div>
                <br>
            </c:forEach>

            <h3>Your result: ${numberOfRightAnswers} from ${numberOfTasks}</h3>

            <br>
            <br>

        </div>

        <hr>
    </div>
</main>
<%@ include file="common/footer.jsp"%>
<%@ include file="common/scripts.jsp"%>
</body>
</html>

