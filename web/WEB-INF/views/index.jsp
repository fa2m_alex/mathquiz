<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html lang="en">

<%@ include file="common/head.jsp"%>

<body>

<%@ include file="common/header.jsp"%>

<main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Math Quiz</h1>
            <p>Math quiz web application based on ontology and description logic.</p>
            <%--<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>--%>
            <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-4">
                <h2>Derivatives</h2>
                <p>Test your knowledge of derivatives.</p>
                <%--<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>--%>
                <p><a class="btn btn-secondary" href="/task?type=derivatives" role="button">Go to test &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>Integrals</h2>
                <p>Test your knowledge of integrals.</p>
                <%--<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>--%>
                <p><a class="btn btn-secondary" href="#" role="button">Go to test &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>Logarithms</h2>
                <p>Test your knowledge of logarithms.</p>
                <%--<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>--%>
                <p><a class="btn btn-secondary" href="#" role="button">Go to test
                    &raquo;</a></p>
            </div>
        </div>

        <hr>

    </div> <!-- /container -->

</main>

<%@ include file="common/footer.jsp"%>
<%@ include file="common/scripts.jsp"%>

</body>
</html>

