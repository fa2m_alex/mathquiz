<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<%@ include file="common/head.jsp"%>
<body>
<%@ include file="common/header.jsp"%>
<main>
    <div class="container">
        <div class="col-md-12">
            <br>
            <h3>${user.email}</h3>
            <h4>Problem tasks:</h4>
            <c:forEach var="task" items="${user.problemTasks}">
                <div class="card">
                    <div class="card-header">${task.task}</div>
                    <div class="card-body">
                        <a class="btn btn-dark" href="/learn?task_id=${task.individual}" role="button">Learn</a>
                    </div>
                </div>
                <br>
            </c:forEach>

            <br>
            <br>

        </div>

        <hr>
    </div>
</main>
<%@ include file="common/footer.jsp"%>
<%@ include file="common/scripts.jsp"%>
</body>
</html>

