package com.selivanov.dao;

import com.selivanov.model.OntologyData;
import com.selivanov.model.OntologyTask;
import com.selivanov.model.User;
import org.apache.jena.base.Sys;
import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.mindrot.jbcrypt.BCrypt;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.apache.jena.ontology.OntModelSpec.OWL_MEM;
import static org.apache.jena.ontology.OntModelSpec.OWL_MEM_MICRO_RULE_INF;

public class OntologyDataDAO {
    private final String SOURCE = "http://www.semanticweb.org/alexf/ontologies/2018/3/MathQuiz";
    private final String NS = SOURCE + "#";
    private final String PATH = "C:\\mathquizOntology.owl";
    //private String PATH = "derivatives.owl";

    //private ArrayList<OntologyTask> tasks;
    private OntModel baseModel;
    private OntModel dlModel;

    public OntologyDataDAO() {
        this.baseModel = ModelFactory.createOntologyModel(OWL_MEM);
        this.baseModel.read(PATH, "RDF/XML");
        this.dlModel = ModelFactory.createOntologyModel(OWL_MEM_MICRO_RULE_INF, baseModel);
    }

    public OntologyTask getTaskById(String id){
        Individual individual = dlModel.getIndividual(NS + id);
        OntologyTask returnTask = new OntologyTask();

        returnTask.setIndividual(id);

        Property pr1 = dlModel.getProperty(NS + "task");
        returnTask.setTask(individual.getProperty(pr1).getString());

        ArrayList<String> answers = new ArrayList<String>();

        Property pr2 = dlModel.getProperty(NS + "answer");

        for (StmtIterator j = individual.listProperties(pr2); j.hasNext(); ) {
            Statement statement = j.next();
            answers.add(statement.getString());
        }

        returnTask.setAnswers(answers);

        Property pr3 = dlModel.getProperty(NS + "correctAnswer");
        returnTask.setCorrectAnswer(individual.getProperty(pr3).getString());

        return returnTask;
    }

    public ArrayList<OntologyTask> generateTasksByTopic(String topic){
        ArrayList<OntologyTask> returnArray = new ArrayList<OntologyTask>();

        OntClass d = dlModel.getOntClass(NS + topic);

        for (ExtendedIterator<Individual> i = (ExtendedIterator<Individual>) d.listInstances(); i.hasNext(); ) {
            Individual temp = i.next();
            returnArray.add(getTaskById(temp.getLocalName()));
        }

        return returnArray;
    }

    public ArrayList<OntologyTask> getTasksByProperty(ArrayList<OntologyTask> tasks, String propertyStr){
        ArrayList<OntologyTask> returnArray = new ArrayList<OntologyTask>();

        for (OntologyTask task:tasks) {
            Individual individual = dlModel.getIndividual(NS + task.getIndividual());

            Property predicate = dlModel.getProperty(NS + propertyStr);
            List<Statement> listOfStatement1 = individual.listProperties(predicate).toList();

            if(listOfStatement1.size() > 1)
                for(int i = 0; i < listOfStatement1.size(); i++) {
                    Statement statement = listOfStatement1.get(i);
                    Individual tempInd = dlModel.getIndividual(statement.getObject().toString());

                    OntologyTask tempTask = getTaskById(tempInd.getLocalName());

                    if(!containsName(returnArray, tempTask.getIndividual()))
                        returnArray.add(tempTask);
                }
        }

        return returnArray;
    }

    public int registerNewUser(User user){
        OntClass userClass = baseModel.getOntClass(NS + "User");

        if(checkUserNotInBase(user)){
            Individual newUser = baseModel.createIndividual(NS + user.getEmail(), userClass);
            Property passwordProperty = baseModel.getProperty(NS + "password");
            String encryptedPassword = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
            newUser.addProperty(passwordProperty, encryptedPassword);

            saveBaseModel();
            return 0;
        }

        return -1;
    }

    public User getUserByEmail(String email){
        Individual userIndividual = dlModel.getIndividual(NS + email);

        System.out.println();

        User retUser = new User();
        retUser.setEmail(email);

        Property passwordProperty = dlModel.getProperty(NS + "password");
        retUser.setPassword(userIndividual.getProperty(passwordProperty).getString());

        Property doesntKnowProperty = dlModel.getProperty(NS + "doesntKnow");
        List<Statement> problemTasks = userIndividual.listProperties(doesntKnowProperty).toList();

        ArrayList<OntologyTask> problemTaskArray = new ArrayList<>();

        for (int i = 0; i < problemTasks.size(); i++) {
            Statement statement = problemTasks.get(i);
            Individual tempInd = dlModel.getIndividual(statement.getObject().toString());

            OntologyTask tempTask = getTaskById(tempInd.getLocalName());
            problemTaskArray.add(tempTask);
        }

        retUser.setProblemTasks(problemTaskArray);

        return retUser;
    }

    public void addProblemTaskToUser(User user, OntologyTask task){
        Individual userIndividual = dlModel.getIndividual(NS + user.getEmail());
        Individual taskIndividual = dlModel.getIndividual(NS + task.getIndividual());

        Property problemTask = baseModel.getProperty(NS + "doesntKnow");
        userIndividual.addProperty(problemTask, taskIndividual);

        saveBaseModel();
    }

    public void removeFromProblemTasks(User user, OntologyTask task){
        Individual userIndividual = dlModel.getIndividual(NS + user.getEmail());
        Individual taskIndividual = dlModel.getIndividual(NS + task.getIndividual());

        Property problemTask = baseModel.getProperty(NS + "doesntKnow");

        userIndividual.removeProperty(problemTask, taskIndividual);

        saveBaseModel();
    }

    private boolean checkUserNotInBase(User user){
        User tmp = null;

        try {
            tmp = getUserByEmail(user.getEmail());
        }catch (NullPointerException e){
            return true;
        }

        return false;
    }

    private void saveBaseModel(){
        try {
            baseModel.write(new FileWriter(PATH), "RDF/XML");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean containsName(final ArrayList<OntologyTask> list, final String name){
        return list.stream().filter(o -> o.getIndividual().equals(name)).findFirst().isPresent();
    }
}
