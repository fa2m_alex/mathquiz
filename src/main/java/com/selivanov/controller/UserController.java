package com.selivanov.controller;

import com.selivanov.model.User;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public ModelAndView profile(){
        ModelAndView modelAndView = new ModelAndView("profile");


        if(TaskController.tempUser == null)
            return new ModelAndView("error");

        User user = TaskController.dao.getUserByEmail(TaskController.tempUser.getEmail());

        modelAndView.addObject("user", user);

        return modelAndView;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView registerForm(){
        ModelAndView modelAndView = new ModelAndView("register");
        modelAndView.addObject("user", new User());

        return modelAndView;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView submit(User user){
        int err;

        ModelAndView modelAndView = new ModelAndView("successRegister");
        modelAndView.addObject("user", user);

        err = TaskController.dao.registerNewUser(user);

        if(err == 0){
            return modelAndView;
        }
        else {
            return new ModelAndView("redirect:/register");
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView loginForm(){
        ModelAndView modelAndView = new ModelAndView("login");
        modelAndView.addObject("user", new User());

        return modelAndView;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView loginPost(User user){
        ModelAndView modelAndView = new ModelAndView("login");

        User tmp = null;

        try {
            tmp = TaskController.dao.getUserByEmail(user.getEmail());
        }catch (NullPointerException e){
            return modelAndView;
        }

        if(!BCrypt.checkpw(user.getPassword(), tmp.getPassword()))
            return modelAndView;

        TaskController.tempUser = tmp;
        TaskController.login = true;

        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logout(){
        ModelAndView modelAndView = new ModelAndView("redirect:/");

        TaskController.tempUser = null;
        TaskController.login = false;

        return modelAndView;
    }
}
