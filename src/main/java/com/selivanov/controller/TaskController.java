package com.selivanov.controller;

import com.selivanov.dao.OntologyDataDAO;
import com.selivanov.model.OntologyData;
import com.selivanov.model.OntologyTask;
import com.selivanov.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

@Controller
public class TaskController {

    public static OntologyDataDAO dao = new OntologyDataDAO();

    public static User tempUser = null;
    public static boolean login = false;

    @RequestMapping(value = "/task", method = RequestMethod.GET)
    public ModelAndView tasksForm(String type){
        ModelAndView modelAndView = new ModelAndView("task");
        modelAndView.addObject("taskType", type);

        OntologyData ontologyData = new OntologyData();
        ArrayList<OntologyTask> tasks;

        if(login){
            if(type!=null && type.equals("derivatives")){
                tasks = dao.generateTasksByTopic("SumOfDerivative");
                ontologyData.setTasks(tasks);
            }
            else if(type!=null && type.equals("integrals")){
                tasks = dao.generateTasksByTopic("SumOfDerivative");
                ontologyData.setTasks(tasks);
            }
            else {
                return new ModelAndView("error");
            }
        }
        else {
            return new ModelAndView("error");
        }

        modelAndView.addObject("data", ontologyData);

        return modelAndView;
    }

    @RequestMapping(value = "/task", method = RequestMethod.POST)
    public ModelAndView submit(OntologyData data){
        ModelAndView modelAndView = new ModelAndView("answer");
        modelAndView.addObject("data", data);

        String temp_task = null;
        modelAndView.addObject("temp_task", temp_task);

        ArrayList<OntologyTask> wrongAnswers = new ArrayList<OntologyTask>();
        ArrayList<OntologyTask> correctAnswers = new ArrayList<OntologyTask>();

        for (OntologyTask task:data.getTasks()) {
            if(!task.isCorrect()){
                wrongAnswers.add(task);
                dao.addProblemTaskToUser(tempUser, task);
            }
            else {
                dao.removeFromProblemTasks(tempUser, task);
            }
        }

        modelAndView.addObject("numberOfRightAnswers", data.getTasks().size() - wrongAnswers.size());
        modelAndView.addObject("numberOfTasks", data.getTasks().size());

        return modelAndView;
    }

    @RequestMapping(value = "/learn", method = RequestMethod.GET)
    public ModelAndView learnForm(String task_id){
        ModelAndView modelAndView = new ModelAndView("learn");

        OntologyData ontologyData = new OntologyData();
        OntologyTask task = dao.getTaskById(task_id);

        ArrayList<OntologyTask> arr = new ArrayList<>();
        arr.add(task);
        ontologyData.setTasks(dao.getTasksByProperty(arr, "hasRelativeTask"));

        modelAndView.addObject("data", ontologyData);
        modelAndView.addObject("task_def", task.getTask());

        return modelAndView;
    }

}
