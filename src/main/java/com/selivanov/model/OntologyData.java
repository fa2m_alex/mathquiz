package com.selivanov.model;

import java.util.ArrayList;

public class OntologyData {
    private ArrayList<OntologyTask> tasks;

    public ArrayList<OntologyTask> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<OntologyTask> tasks) {
        this.tasks = tasks;
    }
}
