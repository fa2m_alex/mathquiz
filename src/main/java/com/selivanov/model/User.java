package com.selivanov.model;

import java.util.ArrayList;

public class User {
    private String email;
    private String password;
    private ArrayList<OntologyTask> problemTasks;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<OntologyTask> getProblemTasks() {
        return problemTasks;
    }

    public void setProblemTasks(ArrayList<OntologyTask> problemTasks) {
        this.problemTasks = problemTasks;
    }
}
