package com.selivanov.model;

import org.apache.jena.ontology.Individual;

import java.util.ArrayList;

public class OntologyTask {
    String task;
    ArrayList<String> answers;
    String answer;
    String correctAnswer;
    String individual;

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public boolean isCorrect() {
        if(answer!=null && answer.equals(correctAnswer))
            return true;
        else
            return false;
    }

    public String getIndividual() {
        return individual;
    }

    public void setIndividual(String individual) {
        this.individual = individual;
    }
}
